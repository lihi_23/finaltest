<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $fillable = ['date','text','candidate_id','user_id'];

    public function candidates()
    {
        return $this->belongsTo('App\Candidate');
    } 

    public function users()
    {
        return $this->belongsTO('App\User');

    }





    
}

