<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
        
        'date' => '2020-07-15',
        'text' => Str::random(15),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
        
        ]);
    }
}
