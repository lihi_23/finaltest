@extends('layouts.app')

@section('title', 'Create Interview')

@section('content')
        <h1>Create Interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "name">interview date</label>
            <input type = "text" class="form-control" name = "date">
        </div>     
        <div class="form-group">
            <label for = "email">interview summary</label>
            <input type = "text" class="form-control" name = "text">
        </div> 
        <div class="form-group">
            <label>candidate interview</label>

            <input type = "text" class="form-control" name = "candidate_id">
        </div>
    
        <div class="form-group">
            <label>Interviewer</label>
            <input type = "text" class="form-control" name = "user_id">
        </div>

        <div>
            <input type = "submit" name = "submit" value = "Create interview">
        </div>                       
        </form>    
@endsection
