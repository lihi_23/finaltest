@extends('layouts.app')

@section('title', 'Interviews')

@section('content')


<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
<h1>List of interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Date</th><th>Text</th><th>Candidate ID</th><th>User ID</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->date}}</td>
            <td>{{$interview->text}}</td>
            <td>{{$interview->candidate_id}}</td>
            <td>{{$interview->user_id}}</td>
            

        
        </tr>
                
    @endforeach
</table>
@endsection

